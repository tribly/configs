# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="mine"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias ls='ls -v --color=auto'
alias ..="cd .."
alias gcc="gcc -Wall -pedantic -ggdb -std=c99"
alias packern="packer --noedit"
alias grep="grep --color -n"
alias grepc="grep --color -n -A 2 -B 2"
alias chromium="chromium --disk-cache-dir=/tmp --memory-model=low"
alias scrots="scrot --delay 5 -c -q 100"
alias aria="aria2c --truncate-console-readout --summary-interval=0"
alias push_uni="rsync -a --progress --compress-level=9 ~/uni uberspace:"
alias pull_uni="rsync -a --progress --compress-level=9 uberspace:uni ~/"
alias homepage_push="rsync -ahz --progress --delete --compress-level=9 --exclude='.git' ~/websites/homepage/ uberspace:html/"
alias ssh='eval $(/usr/bin/keychain --eval --agents ssh -Q --quiet ~/.ssh/tribly) && ssh'


# Set to this to use case-sensitive completion
CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how many often would you like to wait before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(gitfast)

source $ZSH/oh-my-zsh.sh
unsetopt correct_all
setopt nohashdirs

# Customize to your needs...
export PATH=$PATH:/home/heinz/scripts
