execute pathogen#infect()
 
let g:molokai_original = 1
let g:Powerline_symbols='fancy'

syntax on

filetype plugin on
filetype indent on

set undodir=~/.vim/undodir
set undofile
set undolevels=1000
set undoreload=1000

set rtp+=/home/heinz/.vim/bundle/powerline/powerline/bindings/vim
set number
set nowrap
set t_Co=256
set expandtab
set cul
set shiftwidth=4
set tabstop=4
set softtabstop=4
set autoindent
set cpoptions+=$
set noshowmode
set laststatus=2
set encoding=utf-8
set wildignore=*.class
set scrolloff=2


colorscheme molokai

if ! has('gui_running')
    set ttimeoutlen=10
    augroup FastEscape
        autocmd!
        au InsertEnter * set timeoutlen=0
        au InsertLeave * set timeoutlen=1000
    augroup END
endif

:map <F2> :set number!<CR>
:map <F3> :set paste!<CR>
