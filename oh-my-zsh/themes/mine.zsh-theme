if [ -n "$SSH_CONNECTION" ]
then

PROMPT='

%{$fg_bold[green]%}%n%{$reset_color%} at %{$fg_bold[yellow]%}%m%{$reset_color%} in %{$fg_bold[orange]%}%~%{$reset_color%}
$(prompt_char) '
RPROMPT='%T'

else

GIT_PS1_SHOWUNTRACKEDFILES=true
GIT_PS1_SHOWDIRTYSTATE=true

PROMPT='

%{$fg_bold[green]%}%n%{$reset_color%} at %{$fg_bold[yellow]%}%m%{$reset_color%} in %{$fg_bold[orange]%}%~%{$reset_color%}$(__git_ps1 " (%s)")
$(prompt_char) '
RPROMPT='%T'

fi

function prompt_char {
    git branch >/dev/null 2>/dev/null && echo '♓' && return
    echo '>'
}
